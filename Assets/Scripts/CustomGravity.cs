﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class CustomGravity : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public float pullRadius;

    public float gravitatinalPull;

    public float minRadius;

    public float distanceMultiplier;

    public LayerMask layerToPull;


    #endregion

    #region Unity Messages

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled
    /// </summary>
    private void FixedUpdate()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, pullRadius, layerToPull);

        foreach (var col in colliders)
        {
            Rigidbody rb = col.GetComponent<Rigidbody>();

            if (rb == null) continue;

            Vector3 direction = transform.position - col.transform.position;

            if (direction.magnitude < minRadius) continue;

            float distance = direction.sqrMagnitude * distanceMultiplier + 1;



            rb.AddForce(direction.normalized * (gravitatinalPull / distance) * rb.mass * Time.fixedDeltaTime); 
        }
    }

    #endregion

    #region Methods



    #endregion

}
